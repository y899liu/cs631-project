import re,sys
import argparse
import scipy.stats

import numpy as np
import matplotlib.pyplot as plt

# Fixing random state for reproducibility
np.random.seed(19680801)

def load_run(file):
    run = {}
    with open(file, 'r') as f:
        for line in f:
            line = line.strip()
            topic, _, docid, _, _, _ = re.split(' ', line)
            #print(f'{topic}--{docid}')
            if topic not in run:
                run[topic] = []
            run[topic].append(docid)

    return run


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("[Usage] python3 compute_kendalls_tau.py {file1} {file2} {graph_title}")
        raise Exception("Invalid arguments")

    run1 = load_run(sys.argv[1])
    run2 = load_run(sys.argv[2])
    title = sys.argv[3]
    data = {}
    for k in [10, 20, 50, 100, 200, 500, 1000]:
        data[k] = []
        for topic in run1:
            #if topic == '348':
            #    continue
            tau, p_value = scipy.stats.kendalltau(run1[topic][:k], run2[topic][:k])
            print(f'{topic} {tau}')
            data[k].append(tau)

    data2 = []
    for topic in run1:
        #if topic == '348':
        #    continue
        tau, p_value = scipy.stats.kendalltau(run1[topic], run2[topic])
        print(f'{topic} {tau}')
        data2.append(tau)

    fig1, ax1 = plt.subplots()
    ax1.boxplot([data[10], data[20], data[50], data[100], data[200], data[500], data[1000]])
    ax1.set_xticklabels(['10', '20', '50', '100', '200', '500', '1000'])
    ax1.set_ylabel(r"Kendall's $\tau$")
    ax1.set_xlabel(r'Ranked List Depth ($k$)')
    fig1.suptitle(title, fontsize=8)
    fig1.savefig("tau-analysis.pdf", bbox_inches='tight')
