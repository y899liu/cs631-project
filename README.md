# CS631 Project IBM Model 1 Reranker for MS-MARCO Passage Ranking

## Dataset
The dataset used for this project is MSMARCO Passage dataset (https://microsoft.github.io/msmarco/)

## Script

1. First step is to train a IBM model using Train_vcb.ipynb.  A translation table for each query token and document token pairs is generated.
2. Second step is to run the ibm model and interpolate the result with original dat using IBM_Model1_reranker.ipynb.
3. By reranking, the original file run.msmarco-passage.bm25tuned.trec is converted to ibm-reranker.trec.txt

## Results Analysis

### Pipeline

![Pipeline](results/pipeline.png)




### Results table

![Results table](results/results-table.png)

### Results per-topic


![Results per-topic](results/results-per-topic.png)